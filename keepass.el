;;; keepass.el --- Retrieve passwords from KeePass -*- lexical-binding: t; -*-
;;
;; Author: Taylor Grinn <grinntaylor@gmail.com>
;; File: keepass.el
;; Package-Requires: ((emacs "25.1"))
;; Version: 1.0.0
;; URL: https://gitlab.com/tay-dev/keepass.el
;;
;;; Commentary:
;;
;; This package provides the command `keepass-picker' to quickly
;; retrieve selected KeePass passwords from within Emacs using
;; `keepassxc-cli'.
;;
;; ------------------------------------------------------------------
;;                              Setup
;; ------------------------------------------------------------------
;;
;; This package uses the command keepassxc-cli, which must located on
;; the path.  Install KeePassXC in order to get this program.
;;
;; Set the absolute path to your KeePass database file:
;;
;;  (setq keepass-db "/absolute/path/to/my-passwords.kdbx")
;;
;; If you use a key file to unlock the database, set it as
;; `keepass-key-file'.
;;
;; A password can be retrieved by using the command `keepass-picker'.
;; In order to create a global keybinding for `keepass-picker', use
;; the utility function `keepass-global' in your config.
;;
;;  (keepass-global "C-c p")
;;
;; OR
;;
;;  (keepass-global (kbd "C-c p"))
;;
;;
;; ------------------------------------------------------------------
;;                      Registering passwords
;; ------------------------------------------------------------------
;;
;; In order to register a password for the password manager, use the
;; function `keepass-register':
;;
;;  (keepass-register "m" "My Pass" :name "Email/Yahoo Email")
;;
;; This will register a password entry named "My Pass" which can be
;; retrieved by calling `keepass-picker' and entering the letter "m".
;;
;; `keepass-register' can also be used to register a category of
;; passwords by omitting the KeePass name:
;;
;;  (keepass-register "e" "Email category")
;;
;; After a category has been registered, passwords can be registered
;; under the category by prepending the key of the category to the key
;; of the password:
;;
;;  (keepass-register "el" "Laura's email" :name "Laura Gmail")
;;
;; Categories can be nested arbitrarily deep:
;;
;;  (keepass-register "s" "Social")
;;  (keepass-register "sr"  "Reddit")
;;  (keepass-register "srp"   "Personal" :name "Reddit/Personal")
;;  (keepass-register "sra"   "Anon"     :name "Reddit/Anon")
;;
;; ------------------------------------------------------------------
;;                            Security
;; ------------------------------------------------------------------
;;
;; The master password for the KeePass database will be cached for
;; `password-cache-expiry' seconds.
;;
;; By default, keepass.el will copy the password to the kill ring for
;; `keepass-timeout' seconds, then remove it.  During this time, the
;; clipboard feature of Emacs will be disabled.  Set
;; `keepass-clipboard' to `t' to allow other applications to access
;; the password through the clipboard.
;;
;; ------------------------------------------------------------------
;;                        Multiple databases
;; ------------------------------------------------------------------
;;
;; When registering a password entry or category, you can specify the
;; database file and/or key file:
;;
;;  (keepass-register "w" "Work Database"
;;    :db "/absolute/path/to/work-passwords.kdbx"
;;    :key-file "/absolute/path/to/key-file")
;;
;; These settings will be applied to all children of the category "w"
;; (unless overriden by the child).
;;
;; To unset the key file for a particular password entry or category,
;; use -1 (setting `nil' will use inheritance).
;;
;;  (keepass-register "w" "Work Database"
;;    :db "/absolute/path/to/work-passwords.kdbx"
;;    :key-file -1)

;;; Code:

;;;; Requirements:

(require 'cl-lib)


;;;; Options

(defgroup keepass nil "Customization options for keepass.el."
  :group 'applications)

(defcustom keepass-db nil
  "The KeePass database to use."
  :type '(choice (const :tag "None" nil)
                 (file :must-match t)))

(defcustom keepass-key-file nil
  "The KeePass key file to unlock the database."
  :type '(choice (const :tag "None" nil)
                 (file :must-match t)))

(defcustom keepass-timeout 10
  "Number of seconds a password will be stored in the kill ring."
  :type 'number)

(defcustom keepass-clipboard nil
  "Whether or not to use the clipboard when copying passwords."
  :type 'boolean)

;;;; Types

(cl-defstruct keepass desc name db key-file)

;;;; Variables

(defvar keepass--keepasses (make-hash-table) "Registered KeePass passwords.")

(defvar keepass--output nil "Raw output of keepassxc-cli.")

(defvar keepass--time nil "Time left for password to reside in `kill-ring'.")
(defvar keepass--timer nil "Timer object used to temporarily kill password.")

(defvar keepass--mode-line-string nil "Message displayed in the mode line.")
(put 'keepass--mode-line-string 'risky-local-variable t)


;;;; keepass-retrieval-mode

(defvar keepass-retrieval-mode-map (make-sparse-keymap))

;;;###autoload
(define-minor-mode keepass-retrieval-mode
  "Use keepassxc-cli to retrieve passwords from a KeePass database."
  :global t
  :keymap keepass-retrieval-mode-map)


;;;; Commands

;;;###autoload
(defun keepass-picker (&optional table)
  "Choose a registered password.

TABLE parameter is used internally."
  (interactive)
  (setq table (or table keepass--keepasses))
  (let ((prompt "\n")
        chars)
    (maphash
     (lambda (key val)
       (when (characterp key)
         (push key chars)
         (setq prompt
               (concat prompt
                       (format "(%c) %s\n"
                               key
                               (if (keepass-p val)
                                   (keepass-desc val)
                                 (gethash 'description val)))))))
     table)
    (if chars
        (let* ((read-char-choice-use-read-key t)
               (val (gethash (read-char-choice prompt chars) table)))
          (message nil)
          (if (keepass-p val)
              (keepass--query val)
            (keepass-picker val)))
      (message "No passwords to pick from in this category"))))


;;;; Functions

;;;###autoload
(defun keepass-global (key-sequence)
  "Set KEY-SEQUENCE to be the global keybinding for `keepass-picker'.

This turns on the global minor mode `keepass-retrieval-mode'."
  (setcdr keepass-retrieval-mode-map nil)
  (if (stringp key-sequence) (setq key-sequence (kbd key-sequence)))
  (define-key keepass-retrieval-mode-map key-sequence #'keepass-picker)
  (keepass-retrieval-mode 1))

;;;###autoload
(cl-defun keepass-register (keys desc &key name db key-file)
  "Register a password or category of passwords associated with string KEYS.

DESC is displayed next to the key when picking a password.

NAME is the KeePass name of the entry you would like to copy the
password from.  If ommitted, a new category will be associated
with KEYS.

DB is the KeePass database file to query for NAME.  `keepass-db'
is used by default if this is ommitted.

KEY-FILE is the key file used to unlock the database.
`keepass-key-file' is used by default if this is ommitted.
KEY-FILE can be set to -1 to disable the key-file even when
`keepass-key-file' is set.

KEYS can be a single character string, in which case it will be
added as a top-level choice.  If KEYS is multiple characters
long, all but the last character will be used to retrieve an
existing category, and the last character will be used as a new
key."
  (declare (indent 2))
  (let ((table keepass--keepasses)
        (address (substring keys 0 (1- (length keys))))
        (key (aref keys (1- (length keys))))
        parent-db parent-key-file)
    (mapc
     (lambda (k)
       (setq table (gethash k table))
       (unless (hash-table-p table)
         (user-error "Category '%c' does not exist" k))
       (if-let ((c-db (gethash 'db table)))
           (setq parent-db c-db))
       (if-let ((c-key-file (gethash 'key-file table)))
           (setq parent-key-file c-key-file)))
     address)
    (if name
        (puthash key
                 (make-keepass :desc desc
                               :name name
                               :db (or db parent-db)
                               :key-file (or key-file parent-key-file))
                 table)
      (let ((new-table (make-hash-table)))
        (puthash 'description desc new-table)
        (if db (puthash 'db db new-table))
        (if key-file (puthash 'key-file key-file new-table))
        (puthash key new-table table)))))

(defun keepass-unregister (keys)
  "Unregister KEYS from registered `keepass--keepasses'.

KEYS can be a single character string, in which case it will
remove a top-level choice.  If KEYS is multiple characters long,
all but the last character will be used to retrieve an existing
category, and the entry associated with the last character will
be removed from that category."
  (let ((table keepass--keepasses)
        (address (substring keys 0 (1- (length keys))))
        (key (aref keys (1- (length keys)))))
    (mapc (lambda (k) (setq table (gethash k table))) address)
    (remhash key table)))


;;;; Utility expressions

(defmacro keepass--seth (sym val)
  "Set SYM to VAL, saving the historical value of SYM."
  `(progn
     (if (boundp ',sym)
         (push (symbol-value ',sym) (get ',sym 'variable-history)))
     (setq ,sym ,val)))

(defmacro keepass--poph (sym)
  "Pop the historical value of SYM."
  `(setq ,sym (pop (get ',sym 'variable-history))))

(defun keepass--update-mode-line (desc)
  "Update mode line with DESC."
  (setq keepass--mode-line-string
        (propertize
         (format "KP: %s (%d)" desc keepass--time)
         'face 'mode-line-highlight))
  (walk-windows
   (lambda (win)
     (with-selected-window win
       (when (and mode-line-format
                  (not (and (listp mode-line-format)
                            (assq 'keepass--mode-line-string mode-line-format))))
         (setq mode-line-format (list "" '(keepass--mode-line-string
                                           (" " keepass--mode-line-string " "))
                                      mode-line-format))))))
  (force-mode-line-update t))

(defun keepass--restore-mode-line ()
  "Restore mode line to previous state."
  (setq keepass--mode-line-string nil)
  (force-mode-line-update t))

(defun keepass--kill (pass desc)
  "Kill PASS to the `kill-ring' temporarily.

DESC is the description of the password being killed.

First disable the clipboard, then kill the password, update the
mode line, and set a timer to remove the password from the
`kill-ring' after `keepass-timeout' seconds.  If
`keepass-clipboard' is t, don't disable the clipboard."
  (when keepass--timer
    ;; Clear existing password from kill ring
    (setq keepass--time 0)
    (funcall (timer--function keepass--timer)))
  (keepass--seth select-enable-clipboard keepass-clipboard)
  (kill-new pass)
  (setq keepass--time keepass-timeout)
  (keepass--update-mode-line desc)
  (setq keepass--timer
        (run-with-timer
         1 1
         `(lambda ()
            (setq keepass--time (1- keepass--time))
            (if (> keepass--time 0)
                (keepass--update-mode-line ,desc)
              (cancel-timer keepass--timer)
              (setq keepass--timer nil)
              (setq kill-ring (delete ,pass kill-ring))
              (keepass--poph select-enable-clipboard)
              (keepass--restore-mode-line))))))

(defun keepass--query (keepass)
  "Query keepassxc-cli for KEEPASS.

If the query is successful, the master password will be cached
and the password will be temporariliy killed to the `kill-ring'
using `keepass--kill'."
  (let* ((db (or (keepass-db keepass) keepass-db (user-error "`keepass-db' not set")))
         (key-file (if (eq -1 (keepass-key-file keepass))
                       nil
                     (or (keepass-key-file keepass) keepass-key-file)))
         (name (keepass-name keepass))
         (desc (keepass-desc keepass))
         (master-pass (password-read (concat db " master password: ") db)))
    (process-send-string
     (make-process
      :name "keepassxc-cli"
      :command (append '("keepassxc-cli" "show" "-a" "Password")
                       (if key-file (list "-k" key-file))
                       (list db name))
      :filter (lambda (_ str) (setq keepass--output (string-trim str)))
      :sentinel `(lambda (proc _)
                   (if (> (process-exit-status proc) 0)
                       (user-error keepass--output)
                     (password-cache-add ,db ,master-pass)
                     (keepass--kill keepass--output ,desc)
                     (setq keepass--output nil))))
     (concat master-pass "\n"))))


(provide 'keepass)

;;; keepass.el ends here
